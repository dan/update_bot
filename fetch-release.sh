#!/bin/sh 

set -e

if [ -z "$1" ]
  then
    echo "Usage: ./fetch-release.sh RELEASE-NAME"
    exit 1
fi

mkdir cwtch_dist/$1
cd cwtch_dist/$1

wget https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/$1/Cwtch-$1.dmg
wget https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/$1/cwtch-installer-$1.exe
wget https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/$1/cwtch-$1.apk
wget https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/$1/cwtch-$1.tar.gz

cd ../..


# Cwtch Update Bot

- Creates a persistent profile in ~/.cwtch/bots/update_bot
- Servers the latest version of cwtch from the directory `cwtch_dist`